import tw, { styled } from "twin.macro"

export default styled.div`
  ${tw`absolute left-0`}
  ${({ top }) => `top: ${top || "0"}`}
`
