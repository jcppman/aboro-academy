import tw from "twin.macro"

import Title from "./Title"

export default tw(Title)`
  inline-block bg-primary px-4 py-2
`
