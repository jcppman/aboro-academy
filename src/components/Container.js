import tw from "twin.macro"

export default tw.div`
  w-full py-16 px-4 md:px-8 lg:px-32 lg:py-32
`
