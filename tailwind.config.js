module.exports = {
  purge: [
    "./src/**/*.js",
    "./src/**/*.css",
  ],
  theme: {
    extend: {
      colors: {
        primary: "rgb(24, 144, 150)",
        dark: "rgb(1,7,9)",
        darkt: "rgba(1,7,9,0.65)",
      },
    },
  },
  variants: {},
  plugins: [],
}
