const path = require("path")
const fs = require("fs")
const get = require("lodash/get")
const extractImagePaths = require("./utils").extractImagePaths

const isUnderPageDir = (p) =>
  p.indexOf(path.resolve(__dirname, "src/pages")) !== -1

exports.onCreateWebpackConfig = ({ actions, getConfig }) => {
  // Hack due to Tailwind ^1.1.0 using `reduce-css-calc` which assumes node
  // https://github.com/bradlc/babel-plugin-tailwind-components/issues/39#issuecomment-526892633
  const config = getConfig()
  config.node = {
    fs: "empty",
  }
}

const allImageFile = {
  type: [`File`],
  resolve: async (source, args, context, info) => {
    const allImagePaths = extractImagePaths(source)
    return await context.nodeModel.runQuery({
      type: `File`,
      query: { filter: { relativePath: { in: allImagePaths } } },
    })
  },
}

exports.createResolvers = ({ createResolvers }) => {
  const resolvers = {
    ContentJson: { allImageFile },
    SitePage: {
      contentFile: {
        type: `File`,
        resolve: async (source, args, context, info) => {
          const { componentPath } = source
          if (!isUnderPageDir(componentPath)) {
            return null
          }

          const { language } = source.context
          const pathObj = path.parse(componentPath)
          delete pathObj.base
          pathObj.name = `${pathObj.name}.${language}`
          pathObj.ext = ".json"
          pathObj.dir = path.resolve(pathObj.dir, "../content")
          const contentFilePath = path.format(pathObj)
          const contentFileResult = await context.nodeModel.runQuery({
            type: `File`,
            query: { filter: { absolutePath: { eq: contentFilePath } } },
          })

          return get(contentFileResult, "0")
        },
      },
    },
    Site: { allImageFile },
  }

  createResolvers(resolvers)
}
