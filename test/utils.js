const assert = require("assert")
const utils = require("../utils")

function testExtractImagePaths() {
  assert.deepStrictEqual(utils.extractImagePaths("loading.png"), [
    "loading.png",
  ])
  assert.deepStrictEqual(utils.extractImagePaths("loading.pn"), [])
  assert.deepStrictEqual(
    utils.extractImagePaths(["loading.pn", "images/abc.jpeg"]),
    ["images/abc.jpeg"]
  )
  assert.deepStrictEqual(
    utils.extractImagePaths([
      "loading.pn",
      "images/abc.jpeg",
      { abc: "image.webp", nothing: "ggg" },
    ]),
    ["images/abc.jpeg", "image.webp"]
  )
  assert.deepStrictEqual(
    utils.extractImagePaths({
      content: [1, 2, "some/image.png"],
      locale: "en",
      extraHeader: "another/image.jpeg",
    }),
    ["some/image.png", "another/image.jpeg"]
  )
}

testExtractImagePaths()
